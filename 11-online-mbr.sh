#!/bin/bash
#cfdisk
echo 'type=83' | sudo sfdisk /dev/sda
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
#pacman -Syy
#pacman -S --noconfirm reflector
#reflector --country Portugal --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
pacstrap /mnt base base-devel linux linux-firmware nano
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt /bin/bash <<EOF
echo .
pacman -S --noconfirm grub networkmanager wget
echo Grub, NetworkManager and wget installed
systemctl enable NetworkManager
echo Network Manager enabled
grub-install --target=i386-pc /dev/sda
echo Grub installed.
grub-mkconfig -o /boot/grub/grub.cfg
echo Grub configuration saved.
EOF
exit
echo .
echo . Please type "reboot now"
echo .
