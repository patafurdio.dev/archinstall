#!/bin/bash
date
echo Delete everything on /dev/sda, create DOS partitions and format it...
sgdisk --zap-all /dev/sda
echo 'type=83' | sudo sfdisk /dev/sda
mkfs.ext4 -F /dev/sda1
mount /dev/sda1 /mnt
echo Copying all files to /mnt please wait...
cp -ax / /mnt
sync
echo Done copy.
echo ---
echo Copying vmlinuz...
cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz /mnt/boot/vmlinuz-linux
echo Done copy.
echo ---
echo Changing to root of the new install done.
genfstab -U /mnt >> /mnt/etc/fstab
sync
arch-chroot /mnt /bin/bash <<EOF
sed -i 's/Storage=volatile/#Storage=auto/' /etc/systemd/journald.conf
rm /etc/udev/rules.d/81-dhcpcd.rules
systemctl disable pacman-init.service choose-mirror.service
rm -r /etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm /etc/systemd/scripts/choose-mirror
rm /etc/systemd/system/getty@tty1.service.d/autologin.conf
rm /root/{.automated_script.sh,.zlogin}
rm /etc/mkinitcpio-archiso.conf
rm -r /etc/initcpio
pacman-key --init
pacman-key --populate archlinux
mkinitcpio -P
echo Config done.
echo ---
echo Creating GRUB config
grub-install --target=i386-pc /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
echo GRUB config done.
systemctl enable dhcpcd.service
EOF
sync
umount /dev/sda1
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo ---
echo You can now reboot the system. Type "reboot now"
echo ---
date
#reboot now
