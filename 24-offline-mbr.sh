#!/bin/bash
cfdisk
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
echo Copying all files to /mnt please wait...
cp -ax / /mnt
echo Done copy.
echo Copying vmlinuz...
cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz /mnt/boot/vmlinuz-linux
echo Done copy.
echo .
echo Please execute the 26-offline-mbr.sh now
echo .
echo Changing to root of the new install done.
arch-chroot /mnt /bin/bash
echo .
