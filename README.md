# https://gitlab.com/patafurdio.dev/archinstall

ArchLinux install commands and tips

If you need to install ArchLinux just read, copy or print the desired script.


10-online-mbr.txt - has all steps for an MBR online install

11-online-mbr.sh  - download the script after booting ArchLinux and chmod +x it and execute

22-offline-mbr.sh - this is the script that will install ArchLinux WITHOUT a networking connection


Visit the project wiki for more information.
